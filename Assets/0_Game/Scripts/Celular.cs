﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Celular : MonoBehaviour
{
    public bool linterna = false;
    public Button encendido;
    public Light light;
    public Material mat;
    public bool matbool = false;
    public float bateria = 100;
    public float desgaste;
    public float visible;
    public Image imagen;
    // Start is called before the first frame update
    void Awake()
    {
        mat.DisableKeyword("_EMISSION");
    }

    // Update is called once per frame
    void Update()
    {
        visible = bateria / 100;
        imagen.fillAmount = visible;


        if (linterna == false || bateria <= 0) { 
            light.intensity = 0;
            mat.DisableKeyword("_EMISSION");
         
        }
        if (linterna == true && bateria > 0)
        {
            light.intensity = 1;
            mat.EnableKeyword("_EMISSION");
            bateria = bateria - desgaste *Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            linterna = !linterna;          
        }
        
      
    }
   
}
