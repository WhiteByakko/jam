﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionLuz : MonoBehaviour
{
    // [Range(-200, 200)]
    public float rotacion = -200f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, rotacion, 0)* Time.deltaTime);
    }
}
